import 'dart:io';

bool isPrime(var num){
  for(var i = 2;i<=num/i;i++) {
    if(num%i==0) {
      return false;
    }
  }
  return true;
  
}

void main() {
  print('Input Number :');
  var num = int.parse(stdin.readLineSync()!);
  if(isPrime(num)) {
    print('$num is Prime Number');
  } else {
    print('$num is not Prime Number');
  }

}
